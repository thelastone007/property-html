(function() {
    'use strict';


    angular.module('myApp', [
            'ui.router',
            'ngSanitize',
            'ui.bootstrap',
            'angularGrid',
            'ngAnimate',
            'LocalStorageModule'
        ])

        .constant('API_URL', 'http://192.168.1.133:3001')
        .config(['$qProvider', '$stateProvider', '$httpProvider', '$urlRouterProvider', 'API_URL', 'localStorageServiceProvider',

            function($qProvider, $stateProvider, $httpProvider, $urlRouterProvider, API_URL, localStorageServiceProvider) {
                // RestangularProvider.setBaseUrl(API_URL);
                localStorageServiceProvider.setPrefix('property');
                localStorageServiceProvider.setStorageType('localStorage');

                $stateProvider
                    .state('main', {
                        url: '/main',
                        templateUrl: 'views/main.html'
                    })
                    .state('main.welcome', {
                        url: '/welcome',
                        templateUrl: 'views/welcome.html',
                        controller: 'dashboardCtrl'
                    })
                    .state('main.home', {
                        url: '/home',
                        templateUrl: 'views/home.html',
                        controller: 'homeCtrl'
                    })
                    .state('main.selection', {
                        url: '/selection',
                        templateUrl: 'views/selection.html',
                        controller: 'homeCtrl'
                    })
                    .state('main.interior', {
                        url: '/interior',
                        templateUrl: 'views/interior.html',
                        controller: 'interiorCtrl'
                    })
                    .state('main.kitchen', {
                        url: '/kitchen',
                        templateUrl: 'views/kitchen.html',
                        controller: 'interiorCtrl'
                    })
                    .state('main.client', {
                        url: '/client',
                        templateUrl: 'views/client.html'
                    })
                    .state('main.model', {
                        url: '/model',
                        templateUrl: 'views/model.html'
                    })
                    .state('main.scheme', {
                        url: '/scheme',
                        templateUrl: 'views/scheme.html'
                    })
                    .state('main.exp', {
                        url: '/exp',
                        templateUrl: 'views/experiment.html'
                    })
                    .state('main.exterior', {
                        url: '/exterior',
                        templateUrl: 'views/exterior.html',
                        controller: 'homeCtrl'
                    })


                $urlRouterProvider.otherwise('/main/welcome');


            }
        ])
        .run(function($rootScope, $window, $http) {
            $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {
                $rootScope.goBack = function() {
                    $window.history.back();
                }
            });
        })
        .controller('dashboardCtrl', function($scope, $rootScope, $state) {
            console.log('dashboardCtrl')
            $scope.click = function() {
                console.log('CLICK')
                $scope.slide = 'slideInUp';
                $state.go('main.home');
                // $scope.slide = 'slideInUp';

            }
            $rootScope.$on('$stateChangeStart', function() {
                console.log('here');
                $scope.slide = 'slideInUp';
            });
        })
        .controller('homeCtrl', function($scope, $rootScope, $state, angularGridInstance, $http, API_URL, $timeout) {
            console.log('homeCtrl', API_URL)
            $scope.homesdesign = [];
            $scope.home = false;
            $scope.facades = [];
            $scope.facade = false;
            $scope.floorplans = [];
            $scope.floorplan = false;
            $scope.title = '';

            $scope.data = {};
            $scope.data.home_type_id = null;
            $scope.data.space_type_id = null;
            $scope.data.floorplan_id = null;
            $scope.floordetail = {};

            $http.get(API_URL + '/hometypes').then(function(resp) {
                if (resp.status == 200 && resp.data.status) {
                    _.each(resp.data.data.home_types, function(row) {
                        row.image = 'img/thumbnail.png';
                    });
                    $scope.homesdesign = resp.data.data.home_types;
                    $scope.homecopy = resp.data.data.home_types;
                }
            });

            $http.get(API_URL + '/spaces').then(function(resp) {
                if (resp.status == 200 && resp.data.status) {
                    _.each(resp.data.data.spaces, function(row) {
                        row.image = 'img/thumbnail.png';
                    });
                    $scope.facades = resp.data.data.spaces;
                }
            });

            $http.get(API_URL + '/floorplans').then(function(resp) {
                console.log('resp:', resp);
                if (resp.status == 200 && resp.data.status) {
                    _.each(resp.data.data.floorplans, function(row) {
                        row.image = 'img/floorplan.png';
                    })
                    $scope.floorplans = resp.data.data.floorplans;
                    console.log('$scope.floorplans:', $scope.floorplans);
                }
            });

            $scope.half = true;

            $scope.angularGridoptions = {
                cssGrid: true,
                refreshOnImgLoad: true,
                gutterSize: 10,
            };

            $scope.images = [{
                    image: 'img/thumbnail.png',
                },
                {
                    image: 'img/thumbnail.png'
                },
                {
                    image: 'img/thumbnail.png'
                }, {
                    image: 'img/thumbnail.png'
                }, {
                    image: 'img/thumbnail.png'
                },
                {
                    image: 'img/thumbnail.png'
                },
                {
                    image: 'img/thumbnail.png'
                }, {
                    image: 'img/thumbnail.png'
                }, {
                    image: 'img/thumbnail.png'
                },
                {
                    image: 'img/thumbnail.png'
                },
                {
                    image: 'img/thumbnail.png'
                }, {
                    image: 'img/thumbnail.png'
                }
            ]

            $scope.getDetail = function(name) {
                console.log('diri', angularGridInstance)
                if (name == 'home') {
                    $scope.title = 'Home Designs';
                    // angularGridInstance.home.refresh();
                    $scope.home = true;
                    $scope.homesdesign = $scope.homecopy;
                    // $scope.facade = false;
                    $scope.floorplan = false;
                } else if (name == 'facade') {
                    $scope.title = 'Facade Type';
                    $scope.home = true;
                    $scope.homesdesign = $scope.facades;
                    $timeout(function() {
                        angularGridInstance.home.refresh();
                    }, 500)
                    // $scope.facade = true;
                    // $scope.home = false;
                    // $scope.floorplan = false;
                } else if (name == 'floorplan') {
                    $scope.title = 'Floorplan';
                    $scope.detail = true;
                    $scope.floorplan = true;
                    $scope.facade = false;
                    $scope.home = false;
                }
                $scope.half = !$scope.half;
            };

            $scope.getHomeDesign = function(data) {
                console.log('data:', data);
                if (data.home_type_id) {
                    $scope.data.home_type_id = data.home_type_id;
                    $scope.data.home_name = data.item_name;
                } else if (data.space_type_id) {
                    $scope.data.space_type_id = data.space_type_id;
                    $scope.data.space_name = data.space_name;
                }
            };

            $scope.getFlooplan = function(floor) {
                console.log('floor:', floor);
                $scope.data.floorplan_id = floor.floorplan_id;
                $scope.data.floor_name = floor.plan_name;
            };

            $scope.close = function() {
                $scope.home = false;
                $scope.facade = false;
                $scope.floorplan = false;
                $scope.half = !$scope.half;
                console.log('half:', $scope.half);
            };

            $scope.viewDetails = function(item) {
                if (item) {
                    $scope.floordetail = item.dimensions;
                }
                $scope.detail = !$scope.detail;

                console.log('$scope.floordetail:', $scope.floordetail);
            };

            $scope.start = function() {
                $state.go('main.scheme');
                // if($scope.data.home_name && $scope.data.space_name && $scope.data.floor_name){
                // console.log('$scope.data:', $scope.data);
                // }
            };

            $scope.findDimension = function(name) {
                return _.findIndex($scope.floordetail, function(row) {
                    return row.dimension_name == name;
                });
            };

        })
        .controller('interiorCtrl', function($scope, $rootScope, $state, angularGridInstance, $uibModal) {
            console.log('interiorCtrl')
            $scope.modal = function() {
                var modalInstance = $uibModal.open({
                    animation: true,
                    scope: $scope,
                    templateUrl: './views/interior.modal.html',
                    size: 'sm'
                });

                modalInstance.result.then(function(selectedItem) {
                    $state.reload();
                }, function() {});

                $scope.cancel = function() {
                    modalInstance.dismiss('cancel');
                }
            }
        });

})();